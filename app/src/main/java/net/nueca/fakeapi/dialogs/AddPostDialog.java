package net.nueca.fakeapi.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import net.nueca.fakeapi.R;

/**
 * Created by rhymartmanchus on 17/11/2017.
 */

public class AddPostDialog extends AppCompatDialog {

    public interface OnPostListener {
        void onPost(String title, String body);
    }

    private OnPostListener onPostListener;

    public AddPostDialog(Context context) {
        super(context);
        setCancelable(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Post Something");
        setContentView(R.layout.f_add_post);

        Button btnClose = findViewById(R.id.btnClose);
        Button btnPost = findViewById(R.id.btnPost);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onPostListener != null) {
                    String title = ((EditText)findViewById(R.id.etTitle)).getText().toString();
                    String body = ((EditText)findViewById(R.id.etBody)).getText().toString();

                    onPostListener.onPost(title, body);

                    dismiss();
                }
            }
        });
    }

    public void setOnPostListener(OnPostListener onPostListener) {
        this.onPostListener = onPostListener;
    }
}
