package net.nueca.fakeapi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.nueca.fakeapi.R;
import net.nueca.fakeapi.objects.TODO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rhymartmanchus on 17/11/2017.
 */

public class TODOsAdapter extends RecyclerView.Adapter<TODOsAdapter.ViewHolder> {

    public interface OnChangeStatusListener {
        void onChangeStatus(int position);
    }

    private List<TODO> todoList = new ArrayList<>();
    private Context context;
    private OnChangeStatusListener onChangeStatusListener;

    public TODOsAdapter(Context context) {
        this.context = context;
    }

    public TODOsAdapter(Context context, ArrayList<TODO> todoList) {
        this.context = context;
        this.todoList = todoList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.listitem_todo, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    public void addItem(TODO todo) {
        this.todoList.add(todo);
        notifyItemInserted(this.todoList.size()-1);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TODO todo = todoList.get(position);

        holder.ivStatus.setImageResource(R.drawable.ic_pending);
        Log.e("isCompleted", todo.isCompleted()+"");
        if(todo.isCompleted())
            holder.ivStatus.setImageResource(R.drawable.ic_completed);
        holder.tvTitle.setText(todo.getTitle());
    }

    @Override
    public int getItemCount() {
        return todoList.size();
    }

    public TODO getItem(int position) {
        return todoList.get(position);
    }

    public void setOnChangeStatusListener(OnChangeStatusListener onChangeStatusListener) {
        this.onChangeStatusListener = onChangeStatusListener;
    }

    public void changeStatus(int position, boolean isCompleted) {
        todoList.get(position).setCompleted(isCompleted);
        notifyItemChanged(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView ivStatus;
        TextView tvTitle;
        View root;

        public ViewHolder(View itemView) {
            super(itemView);
            root = itemView;

            ivStatus = itemView.findViewById(R.id.ivStatus);
            tvTitle = itemView.findViewById(R.id.tvTitle);

            root.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if(onChangeStatusListener != null)
                onChangeStatusListener.onChangeStatus(getLayoutPosition());
        }
    }

}
