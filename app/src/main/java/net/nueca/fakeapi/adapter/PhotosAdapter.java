package net.nueca.fakeapi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import net.nueca.fakeapi.fragments.PhotoFragment;
import net.nueca.fakeapi.objects.Photo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rhymartmanchus on 17/11/2017.
 */

public class PhotosAdapter extends FragmentPagerAdapter {

    private List<Photo> photoList = new ArrayList<>();

    public PhotosAdapter(FragmentManager fm) {
        super(fm);

        Photo photo1 = new Photo("accusamus beatae ad facilis cum similique qui sunt", "http://placehold.it/600/92c952");
        Photo photo2 = new Photo("reprehenderit est deserunt velit ipsam", "http://placehold.it/600/771796");
        Photo photo3 = new Photo("officia porro iure quia iusto qui ipsa ut modi", "http://placehold.it/600/24f355");
        Photo photo4 = new Photo("culpa odio esse rerum omnis laboriosam voluptate repudiandae", "http://placehold.it/600/d32776");
        Photo photo5 = new Photo("natus nisi omnis corporis facere molestiae rerum in", "http://placehold.it/600/f66b97");

        photoList.add(photo1);
        photoList.add(photo2);
        photoList.add(photo3);
        photoList.add(photo4);
        photoList.add(photo5);
    }

    @Override
    public Fragment getItem(int position) {
        return PhotoFragment.getInstance(photoList.get(position));
    }

    @Override
    public int getCount() {
        return photoList.size();
    }
}
