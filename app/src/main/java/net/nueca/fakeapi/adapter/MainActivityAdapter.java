package net.nueca.fakeapi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by rhymartmanchus on 16/11/2017.
 */

public class MainActivityAdapter extends RecyclerView.Adapter<MainActivityAdapter.ViewHolder> {

    public interface OnClick {
        void onClick(int position);
    }

    private String[] modules;
    private Context context;
    private OnClick onClick;

    public MainActivityAdapter(Context context, String[] modules) {
        this.modules = modules;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.text1.setText(modules[position]);
    }

    @Override
    public int getItemCount() {
        return modules.length;
    }

    public void setOnClick(OnClick onClick) {
        this.onClick = onClick;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView text1;

        public ViewHolder(View itemView) {
            super(itemView);

            text1 = itemView.findViewById(android.R.id.text1);
            text1.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(onClick != null)
                onClick.onClick(getLayoutPosition());
        }
    }

}
