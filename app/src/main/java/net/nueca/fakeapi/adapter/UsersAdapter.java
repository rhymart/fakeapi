package net.nueca.fakeapi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import net.nueca.fakeapi.R;
import net.nueca.fakeapi.objects.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rhymartmanchus on 17/11/2017.
 */

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    public interface OnUserClickedListener {
        void onClick(User user);
    }

    private List<User> userList = new ArrayList<>();
    private Context context;
    private OnUserClickedListener onUserClickedListener;

    public UsersAdapter(Context context) {
        this.context = context;
    }

    public UsersAdapter(Context context, ArrayList<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.listitem_user, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        User user = userList.get(position);

        holder.tvName.setText(user.getName()+" | "+user.getUsername());
        holder.tvEmail.setText(user.getEmail());
        holder.tvAddress.setText(user.getAddress().generateAddress());

    }

    public void addItem(User user) {
        this.userList.add(user);
        notifyItemInserted(this.userList.size()-1);
    }

    public void setOnUserClickedListener(OnUserClickedListener onUserClickedListener) {
        this.onUserClickedListener = onUserClickedListener;
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvName, tvEmail, tvAddress;
        View root;

        public ViewHolder(View itemView) {
            super(itemView);
            root = itemView;

            tvName = itemView.findViewById(R.id.tvName);
            tvEmail = itemView.findViewById(R.id.tvEmail);
            tvAddress = itemView.findViewById(R.id.tvAddress);


            root.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(onUserClickedListener != null)
                onUserClickedListener.onClick(userList.get(getLayoutPosition()));
        }
    }

}
