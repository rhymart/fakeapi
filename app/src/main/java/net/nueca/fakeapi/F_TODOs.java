package net.nueca.fakeapi;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import net.nueca.fakeapi.adapter.TODOsAdapter;
import net.nueca.fakeapi.objects.TODO;
import net.nueca.fakeapi.objects.User;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rhymartmanchus on 17/11/2017.
 */

public class F_TODOs extends BaseActivity {

    private RequestQueue requestQueue;
    private Gson gson;
    private TODOsAdapter todOsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_todos);

        gson = new Gson();
        requestQueue = Volley.newRequestQueue(this);

        getSupportActionBar().setTitle("TODOs");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView rvTodos = findViewById(R.id.rvTodos);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvTodos.setLayoutManager(linearLayoutManager);
        rvTodos.setHasFixedSize(true);
        rvTodos.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        todOsAdapter = new TODOsAdapter(this);
        todOsAdapter.setOnChangeStatusListener(new TODOsAdapter.OnChangeStatusListener() {
            @Override
            public void onChangeStatus(final int position) {
                AlertDialog.Builder changeStatus = new AlertDialog.Builder(F_TODOs.this);
                changeStatus.setTitle("Change TODO status");
                changeStatus.setMessage((todOsAdapter.getItem(position).isCompleted() ? "Not finished yet with this?" : "Done with this task?"));
                changeStatus.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        todOsAdapter.changeStatus(position, !todOsAdapter.getItem(position).isCompleted());
                    }
                });
                changeStatus.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                changeStatus.show();
            }
        });
        rvTodos.setAdapter(todOsAdapter);

        JsonArrayRequest getTodos = new JsonArrayRequest("https://jsonplaceholder.typicode.com/todos", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for(int i = 0;i < response.length();i++) {
                    try {
                        TODO todo = gson.fromJson(response.getJSONObject(i).toString(), TODO.class);
                        getHelper().getDao(TODO.class).createOrUpdate(todo);

                        todOsAdapter.addItem(todo);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", "onError");

                try {
                    List<TODO> todoList = getHelper().getDao(TODO.class).queryForAll();
                    for(TODO todo : todoList)
                        todOsAdapter.addItem(todo);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        getTodos.setShouldCache(false);
        requestQueue.add(getTodos);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) // When left arrow is tapped.
            finish(); // just go back from the stack of activities
        return super.onOptionsItemSelected(item);
    }

}
