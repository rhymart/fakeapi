package net.nueca.fakeapi.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by rhymartmanchus on 17/11/2017.
 */

@DatabaseTable
public class TODO {

    @DatabaseField(id = true)
    private int id;
    @DatabaseField
    private String title;
    @DatabaseField
    private boolean completed = false;

    public TODO() {
    }

    public TODO(String title, boolean completed) {
        this.title = title;
        this.completed = completed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
