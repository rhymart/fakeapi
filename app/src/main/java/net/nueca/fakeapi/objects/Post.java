package net.nueca.fakeapi.objects;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by rhymartmanchus on 16/11/2017.
 */

@DatabaseTable
public class Post {

    @DatabaseField(id = true)
    private Integer id;

    @DatabaseField
    private String title;

    @DatabaseField
    private String body;

    @DatabaseField(foreign = true, columnName = "user_id", foreignAutoRefresh = true)
    private User user;

    private int userId;

    public Post() {
    }

    public Post(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String displayPlusOperation() {
        return "title: "+title+"\nbody: "+body;
    }

    public String displayConcatOperation() {
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder.append("title: ").append(title).append("\nbody: ").append(body).toString();
    }

    public String displayStringFormat() {
        String str = String.format("title: %s\nbody: %s", title, body);
        return str;
    }

    @Override
    public String toString() {
        return "Post{" +
                "title='" + title + '\'' +
                ", body='" + body + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public User getUser() {
        return user;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
