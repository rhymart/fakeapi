package net.nueca.fakeapi.objects;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by rhymartmanchus on 16/11/2017.
 */

@DatabaseTable
public class User {

    @DatabaseField(id = true)
    private int id;

    @DatabaseField
    private String name;

    @DatabaseField
    private String username;

    @DatabaseField
    private String email;

    @DatabaseField(foreign = true, columnName = "address_id", foreignAutoCreate = true, foreignAutoRefresh = true)
    private Address address;

    @ForeignCollectionField(columnName = "my_posts_fc", eager = true)
    private transient ForeignCollection<Post> myPosts;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ArrayList<Post> getMyPosts() {
        if(myPosts == null)
            return new ArrayList<>();
        return new ArrayList<>(myPosts);
    }
}
