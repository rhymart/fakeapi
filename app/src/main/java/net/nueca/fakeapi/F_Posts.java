package net.nueca.fakeapi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.internal.Excluder;

import net.nueca.fakeapi.adapter.PostsAdapter;
import net.nueca.fakeapi.dialogs.AddPostDialog;
import net.nueca.fakeapi.objects.Post;
import net.nueca.fakeapi.objects.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rhymartmanchus on 16/11/2017.
 */

public class F_Posts extends BaseActivity {

    private PostsAdapter postsAdapter;
    private RecyclerView rvPosts;
    private RequestQueue requestQueue;
    private Gson gson;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_posts);

        requestQueue = Volley.newRequestQueue(this);
        gson = new Gson();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Posts");

        rvPosts = findViewById(R.id.rvPosts);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvPosts.setLayoutManager(linearLayoutManager);
        rvPosts.setHasFixedSize(true);
        rvPosts.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        postsAdapter = new PostsAdapter(this);
        rvPosts.setAdapter(postsAdapter);

        JsonArrayRequest getPosts = new JsonArrayRequest("https://jsonplaceholder.typicode.com/posts", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for(int i = 0;i < response.length();i++) {
                    try {
                        Post post = gson.fromJson(response.getJSONObject(i).toString(), Post.class);
                        User user = getHelper().getDao(User.class).queryBuilder().where().eq("id", post.getUserId()).queryForFirst();
                        post.setUser(user);
                        getHelper().getDao(Post.class).createOrUpdate(post);

                        postsAdapter.addItem(post);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", "onError");

                try {
                    List<Post> postList = getHelper().getDao(Post.class).queryForAll();
                    for(Post post : postList)
                        postsAdapter.addItem(post);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        getPosts.setShouldCache(false);
        requestQueue.add(getPosts);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.f_posts, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) // When left arrow is tapped.
            finish(); // just go back from the stack of activities
        else if(item.getItemId() == R.id.mNewPost) {
            AddPostDialog addPostDialog = new AddPostDialog(this);
            addPostDialog.setOnPostListener(new AddPostDialog.OnPostListener() {
                @Override
                public void onPost(String title, String body) {
                    Post post = new Post(title, body);
                    try {
                        JSONObject postJsonObject = new JSONObject(gson.toJson(post));
                        Log.e("Error", postJsonObject.toString());
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(JsonObjectRequest.Method.POST,
                                "https://jsonplaceholder.typicode.com/posts", postJsonObject,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Post post = gson.fromJson(response.toString(), Post.class);
                                        postsAdapter.addItem(0, post);
                                        rvPosts.scrollToPosition(0);
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e("Volley", "onError: ");
                                    }
                                });
                        jsonObjectRequest.setShouldCache(false);
                        requestQueue.add(jsonObjectRequest);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            addPostDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }
}
