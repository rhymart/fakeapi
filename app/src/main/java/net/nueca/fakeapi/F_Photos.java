package net.nueca.fakeapi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import net.nueca.fakeapi.adapter.PhotosAdapter;

/**
 * Created by rhymartmanchus on 17/11/2017.
 */
public class F_Photos extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.f_photos);

        ViewPager vpPhotos = findViewById(R.id.vpPhotos);

        PhotosAdapter photosAdapter = new PhotosAdapter(getSupportFragmentManager());
        vpPhotos.setAdapter(photosAdapter);

    }

}
