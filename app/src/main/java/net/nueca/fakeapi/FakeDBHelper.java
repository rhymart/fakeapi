package net.nueca.fakeapi;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import net.nueca.fakeapi.objects.Address;
import net.nueca.fakeapi.objects.Post;
import net.nueca.fakeapi.objects.TODO;
import net.nueca.fakeapi.objects.User;

import java.sql.SQLException;

/**
 * Created by rhymartmanchus on 17/11/2017.
 */

public class FakeDBHelper extends OrmLiteSqliteOpenHelper {

    private static String DATABASE_NAME = "fake.db";
    private static int DATABASE_VERSION = 1;

    public FakeDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Address.class);
            TableUtils.createTable(connectionSource, Post.class);
            TableUtils.createTable(connectionSource, TODO.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        int currentVersion = oldVersion;
        while(currentVersion <= newVersion) {
            switch (currentVersion) {
                // YOUR UPDATING METHODS
            }

            currentVersion++;
        }
    }
}
