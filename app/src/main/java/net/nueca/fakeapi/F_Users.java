package net.nueca.fakeapi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import net.nueca.fakeapi.adapter.UsersAdapter;
import net.nueca.fakeapi.objects.Address;
import net.nueca.fakeapi.objects.User;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rhymartmanchus on 17/11/2017.
 */

public class F_Users extends BaseActivity {

    private RequestQueue requestQueue;
    private Gson gson;
    private UsersAdapter usersAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_users);

        requestQueue = Volley.newRequestQueue(this);
        gson = new Gson();

        getSupportActionBar().setTitle("Users");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView rvUsers = findViewById(R.id.rvUsers);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvUsers.setLayoutManager(linearLayoutManager);
        rvUsers.setHasFixedSize(true);
        rvUsers.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        usersAdapter = new UsersAdapter(this);
        usersAdapter.setOnUserClickedListener(new UsersAdapter.OnUserClickedListener() {
            @Override
            public void onClick(User user) {
                Toast.makeText(F_Users.this, "You have "+user.getMyPosts().size()+" posts.", Toast.LENGTH_LONG).show();
            }
        });
        rvUsers.setAdapter(usersAdapter);

        JsonArrayRequest getUsers = new JsonArrayRequest("https://jsonplaceholder.typicode.com/users", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for(int i = 0;i < response.length();i++) {
                    try {
                        User user = gson.fromJson(response.getJSONObject(i).toString(), User.class);
                        User fromDb = getHelper().getDao(User.class).queryBuilder().where().eq("id", user.getId()).queryForFirst();

                        boolean isUserInDatabase = fromDb != null;
                        if(isUserInDatabase)
                            user.setAddress(fromDb.getAddress());
                        else
                            user.getAddress().setUser(user);

                        getHelper().getDao(User.class).createOrUpdate(user);

                        Log.e("DB", "isUserInDatabase: "+isUserInDatabase);
                        Log.e("DB", "Users: "+getHelper().getDao(User.class).countOf());
                        Log.e("DB", "Addresses: "+getHelper().getDao(Address.class).countOf());
                        usersAdapter.addItem(isUserInDatabase ? fromDb : user);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", "onError");

                try {
                    List<User> userList = getHelper().getDao(User.class).queryForAll();
                    for(User user : userList)
                        usersAdapter.addItem(user);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        getUsers.setShouldCache(false);
        requestQueue.add(getUsers);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) // When left arrow is tapped.
            finish(); // just go back from the stack of activities
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OpenHelperManager.releaseHelper();
    }
}
