package net.nueca.fakeapi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import net.nueca.fakeapi.adapter.MainActivityAdapter;
import net.nueca.fakeapi.objects.Post;

public class F_Main extends AppCompatActivity {

    private String[] modules = {"Posts", "Users", "TODOs", "Photos"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_main);

        getSupportActionBar().setTitle("Fake Modules");

        RecyclerView rvMenu = findViewById(R.id.rvMenu);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvMenu.setLayoutManager(linearLayoutManager);
        rvMenu.setHasFixedSize(true);
        rvMenu.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        MainActivityAdapter mainActivityAdapter = new MainActivityAdapter(this, modules);
        mainActivityAdapter.setOnClick(new MainActivityAdapter.OnClick() {
            @Override
            public void onClick(int position) {
                switch (position) {
                    case 0: {
                        Intent intent = new Intent(F_Main.this, F_Posts.class);
                        startActivity(intent);
                    } break;
                    case 1: {
                        Intent intent = new Intent(F_Main.this, F_Users.class);
                        startActivity(intent);
                    } break;
                    case 2: {
                        Intent intent = new Intent(F_Main.this, F_TODOs.class);
                        startActivity(intent);
                    } break;
                    case 3: {
                        Intent intent = new Intent(F_Main.this, F_Photos.class);
                        startActivity(intent);
                    } break;
                }
            }
        });
        rvMenu.setAdapter(mainActivityAdapter);

    }
}
