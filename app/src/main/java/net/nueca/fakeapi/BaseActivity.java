package net.nueca.fakeapi;

import android.support.v7.app.AppCompatActivity;

import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * Created by rhymartmanchus on 17/11/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private FakeDBHelper fakeDBHelper;

    protected FakeDBHelper getHelper() {
        if(fakeDBHelper == null)
            fakeDBHelper = OpenHelperManager.getHelper(this, FakeDBHelper.class);
        return fakeDBHelper;
    }
}
