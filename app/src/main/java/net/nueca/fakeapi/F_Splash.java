package net.nueca.fakeapi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by rhymartmanchus on 17/11/2017.
 */

public class F_Splash extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_splash);

        Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                Intent intent = new Intent(F_Splash.this, F_Main.class);
                finish();
                startActivity(intent);
                return false;
            }
        });
        handler.sendEmptyMessageDelayed(0, 3000);
    }

    @Override
    public void onBackPressed() {

    }
}
