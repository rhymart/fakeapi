package net.nueca.fakeapi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.nueca.fakeapi.R;
import net.nueca.fakeapi.objects.Photo;

/**
 * Created by rhymartmanchus on 17/11/2017.
 */

public class PhotoFragment extends Fragment {

    private Photo photo;

    public static PhotoFragment getInstance(Photo photo) {
        PhotoFragment photoFragment = new PhotoFragment();
        photoFragment.setPhoto(photo);
        return photoFragment;
    }

    public PhotoFragment() {
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_photo, null);

        ImageView ivPhoto = view.findViewById(R.id.ivPhoto);
        TextView tvDescription = view.findViewById(R.id.tvDescription);

        Picasso.with(getContext()).load(photo.getUrl()).into(ivPhoto);
        tvDescription.setText(photo.getTitle());

        return view;
    }
}
